<?php

/**
 * Description of AgreementForm
 *
 * @author robert.gislason
 */
class Application_Form_DeviceForm extends Zend_Form {

    public function __construct($option = null)
    {
        parent::__construct($option);

        //Check if error is set
        if(isset($_GET['error']))
        {
            $errors = array('msg' => $_GET['error']);
            echo "<font color='red'>".$_GET['error']."</font>";
        }

        $this->setName('device');

        $model = new Zend_Form_Element_Text('model');
        $model->setLabel('Model:');

        $product_nr = new Zend_Form_Element_Text('product_nr');
        $product_nr->setLabel('Product nr:');

        $type = new Zend_Form_Element_Text('type');
        $type->setLabel('* Type:');

        $serial_nr = new Zend_Form_Element_Text('serial_nr');
        $serial_nr->setLabel('* Serial nr:');

        $cpu = new Zend_Form_Element_Text('cpu');
        $cpu->setLabel('CPU:');

        $disc = new Zend_Form_Element_Text('disc');
        $disc->setLabel('Disc:');

        $ram = new Zend_Form_Element_Text('ram');
        $ram->setLabel('RAM:');

        $graphics = new Zend_Form_Element_Text('graphics');
        $graphics->setLabel('Graphics:');

        $os = new Zend_Form_Element_Text('os');
        $os->setLabel('OS:');

        $reg_device = new Zend_Form_Element_Submit('reg_device');
        $reg_device->setLabel('Register');

        $this->addElements(array($model, $product_nr, $type, $serial_nr, $cpu, $disc, $ram, $graphics, $os, $reg_device));
        $this->setMethod('post');
        $this->setAction('/registration/process_device');
    }

}
?>
