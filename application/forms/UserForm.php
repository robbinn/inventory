<?php

/**
 * Description of AgreementForm
 *
 * @author robert.gislason
 */
class Application_Form_UserForm extends Zend_Form {

    public function __construct($option = null)
    {
        parent::__construct($option);

        //Check if error is set
        if(isset($_GET['error']))
        {
            $errors = array('msg' => $_GET['error']);
            echo "<font color='red'>".$_GET['error']."</font>";
        }
        
        //TODO: Check if user exists before inserting him and if so use the user id that exists
        $name = mb_convert_encoding($_SESSION['checkuser'], "ISO-8859-1", "UTF-8");
        $userservice = new Application_Service_UserService();
        $existing_user = $userservice->FindUser($name);

        $this->setName('user');

        $real_name = new Zend_Form_Element_Text('real_name');
        $real_name->setLabel('* Full name:');
        $real_name->setValue($existing_user['real_name']);

        $user_name = new Zend_Form_Element_Text('user_name');
        $user_name->setLabel('* Username:');

        $responsible = new Zend_Form_Element_Text('responsible');
        $responsible->setLabel('Responsible:');

        $department = new Zend_Form_Element_Text('department');
        $department->setLabel('Department:');
        
        $reg_user = new Zend_Form_Element_Submit('reg_user');
        $reg_user->setLabel('Next');

        if(!isset($existing_user))
        {
            $name = $_SESSION['checkuser'];
            $encname = mb_convert_encoding($name, "ISO-8859-1", "UTF-8");
            $real_name->setValue($encname);
        }
        else
        {
            $real_name->setValue($existing_user['real_name']);

            $user_name->setValue($existing_user['user_name']);

            $responsible->setValue($existing_user['responsible']);

            $department->setValue($existing_user['department']);
            
            $_SESSION['user_id'] = $existing_user['id'];
        }

        $this->addElements(array($real_name, $user_name, $responsible, $department, $reg_user));
        $this->setMethod('post');
        $this->setAction('/registration/process_user');
        
    }

}
?>
