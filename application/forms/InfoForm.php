<?php

/**
 * Description of AgreementForm
 *
 * @author robert.gislason
 */
class Application_Form_InfoForm extends Zend_Form {

    public function __construct($option = null)
    {
        parent::__construct($option);

        //Check if error is set
        if(isset($_GET['error']))
        {
            $errors = array('msg' => $_GET['error']);
            echo "<font color='red'>".$_GET['error']."</font>";
        }

        $this->setName('info');

        $got_nr = new Zend_Form_Element_Text('got_nr');
        $got_nr->setLabel('* GOT nr:');

        //Use JQuery DatePicker
        $inst_date = new ZendX_JQuery_Form_Element_DatePicker(
                        'inst_date',
                        array('label' => 'Installation date:')
        );

        $host_name = new Zend_Form_Element_Text('host_name');
        $host_name->setLabel('Hostname:');

        $usage = new Zend_Form_Element_Text('usage');
        $usage->setLabel('Usage:');

        $inv_type = new Zend_Form_Element_Text('inv_type');
        $inv_type->setLabel('Inventory type:');

        $manufacturer = new Zend_Form_Element_Text('manufacturer');
        $manufacturer->setLabel('Manufacturer:');

        $maconomy_asset_nr = new Zend_Form_Element_Text('maconomy_asset_nr');
        $maconomy_asset_nr->setLabel('* Maconomy asset nr:');

        $note = new Zend_Form_Element_Text('note');
        $note->setLabel('Note:');

        $password = new Zend_Form_Element_Text('password');
        $password->setLabel('Password:');

        $reg_info = new Zend_Form_Element_Submit('reg_info');
        $reg_info->setLabel('Next');

        $ac_user = new ZendX_JQuery_Form_Element_AutoComplete('ac_user', null);
        $ac_user->setLabel('* User:');

        /*
         * AUTOCOMPLETE FIELD: Get array information about users into setJQueryParam
         */
        $realnames = $_SESSION['allusernames'];
        $namearray = null;
        for($i=0; $i<sizeof($realnames); $i++)
        {
            $namearray[$i] = mb_convert_encoding($realnames[$i]['real_name'], "UTF-8", "ISO-8859-1");

        }
        
        $ac_user->setJQueryParam('data', $namearray);


        $this->addElements(array($got_nr, $inst_date, $host_name, $usage, $inv_type, $manufacturer, $maconomy_asset_nr, $note, $password, $ac_user, $reg_info));
        $this->setMethod('post');
        $this->setAction('/registration/process_info');
    }

}
?>
