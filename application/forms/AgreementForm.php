<?php

/**
 * Description of AgreementForm
 *
 * @author robert.gislason
 */
class Application_Form_AgreementForm extends Zend_Form {

    public function __construct($option = null) {
        parent::__construct($option);
        
        //Check if error is set
        if(isset($_GET['error']))
        {
            $errors = array('msg' => $_GET['error']);
            echo "<font color='red'>".$_GET['error']."</font>";
        }

        $this->setName('agreement');

        $ticket_nr = new Zend_Form_Element_Text('ticket_nr');
        $ticket_nr->setLabel('* Ticket nr:');
        
        $support_agreement = new Zend_Form_Element_Text('support_agreement');
        $support_agreement->setLabel('Support agreement:');


        //Find date 3years ahead from today.
        $currentDate = date("Y-m-d");
        //Add 3 years to current date
        $dateThreeYearsAdded = strtotime(date("Y-m-d", strtotime($currentDate)) . " +3 year");

        //Use JQuery DatePicker
        $support_end = new ZendX_JQuery_Form_Element_DatePicker("support_end",
                                      array('label' => '* Support end:'),
                                      array('defaultDate' =>
                                                1096));
        $support_end->setJQueryParam('dateFormat', 'yy-mm-dd');
        $support_end->setValue(date("Y-m-d", $dateThreeYearsAdded));

        $bought_from = new Zend_Form_Element_Text('bought_from');
        $bought_from->setLabel('Bought from:');

        //Use JQuery DatePicker
        $bought_on = new ZendX_JQuery_Form_Element_DatePicker(
                        'bought_on',
                        array('label' => 'Bought on:')
        );
        $bought_on->setValue($currentDate);

        $order_nr = new Zend_Form_Element_Text('order_nr');
        $order_nr->setLabel('Order nr:');

        $reg_agreement = new Zend_Form_Element_Submit('reg_agreement');
        $reg_agreement->setLabel('Next');

        $this->addElements(array($ticket_nr, $support_agreement, $support_end, $bought_from, $bought_on, $order_nr, $reg_agreement));
        $this->setMethod('post');
        $this->setAction('/registration/process_agreement');

        
    }

}

?>
