<?php

/**
 * Description of User
 *
 * @author robert.gislason
 */
class Application_Model_User extends Zend_Db_Table_Abstract{
    private $id;
    private $real_name;
    private $user_name;
    private $responsible;
    private $department;
    private $consultant;

    function __construct($real_name, $user_name, $responsible, $department, $consultant) {
        $this->real_name = $real_name;
        $this->user_name = $user_name;
        $this->responsible = $responsible;
        $this->department = $department;
        $this->consultant = $consultant;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getReal_name() {
        return $this->real_name;
    }

    public function setReal_name($real_name) {
        $this->real_name = $real_name;
    }

    public function getUser_name() {
        return $this->user_name;
    }

    public function setUser_name($user_name) {
        $this->user_name = $user_name;
    }

    public function getResponsible() {
        return $this->responsible;
    }

    public function setResponsible($responsible) {
        $this->responsible = $responsible;
    }

    public function getDepartment() {
        return $this->department;
    }

    public function setDepartment($department) {
        $this->department = $department;
    }

    public function getConsultant() {
        return $this->consultant;
    }

    public function setConsultant($consultant) {
        $this->consultant = $consultant;
    }


    public function isValid()
    {
        $error = null;

        //Check if the variables are empty
        if ((empty($this->real_name)) || (empty($this->user_name) ) )
        {
            $error = "Please fill in the required (*)fields";
            return $error;
        }

        return true;
    }


}
?>
