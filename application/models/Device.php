<?php

/**
 * Description of Computer
 *
 * @author robert.gislason
 */
class Application_Model_Device extends Zend_Db_Table_Abstract{
    private $id;
    private $user_id;
    private $agreement_id;
    private $info_id;
    private $model;
    private $product_nr;
    private $type;
    private $serial_nr;
    private $cpu;
    private $disc;
    private $ram;
    private $graphics;
    private $os;
    private $active;
    private $onstock;

    function __construct($user_id, $agreement_id, $info_id, $model, $product_nr, $type, $serial_nr, $cpu, $disc, $ram, $graphics, $os, $active, $onstock) {
        $this->user_id = $user_id;
        $this->agreement_id = $agreement_id;
        $this->info_id = $info_id;
        $this->model = $model;
        $this->product_nr = $product_nr;
        $this->type = $type;
        $this->serial_nr = $serial_nr;
        $this->cpu = $cpu;
        $this->disc = $disc;
        $this->ram = $ram;
        $this->graphics = $graphics;
        $this->os = $os;
        $this->active = $active;
        $this->onstock = $onstock;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUser_id() {
        return $this->user_id;
    }

    public function setUser_id($user_id) {
        $this->user_id = $user_id;
    }

    public function getAgreement_id() {
        return $this->agreement_id;
    }

    public function setAgreement_id($agreement_id) {
        $this->agreement_id = $agreement_id;
    }

    public function getInfo_id() {
        return $this->info_id;
    }

    public function setInfo_id($info_id) {
        $this->info_id = $info_id;
    }

    public function getModel() {
        return $this->model;
    }

    public function setModel($model) {
        $this->model = $model;
    }

    public function getProduct_nr() {
        return $this->product_nr;
    }

    public function setProduct_nr($product_nr) {
        $this->product_nr = $product_nr;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getSerial_nr() {
        return $this->serial_nr;
    }

    public function setSerial_nr($serial_nr) {
        $this->serial_nr = $serial_nr;
    }

    public function getCpu() {
        return $this->cpu;
    }

    public function setCpu($cpu) {
        $this->cpu = $cpu;
    }

    public function getDisc() {
        return $this->disc;
    }

    public function setDisc($disc) {
        $this->disc = $disc;
    }

    public function getRam() {
        return $this->ram;
    }

    public function setRam($ram) {
        $this->ram = $ram;
    }

    public function getGraphics() {
        return $this->graphics;
    }

    public function setGraphics($graphics) {
        $this->graphics = $graphics;
    }

    public function getOs() {
        return $this->os;
    }

    public function setOs($os) {
        $this->os = $os;
    }

    public function getActive() {
        return $this->active;
    }

    public function setActive($active) {
        $this->active = $active;
    }

    public function getOnstock() {
        return $this->onstock;
    }

    public function setOnstock($onstock) {
        $this->onstock = $onstock;
    }

    public function isValid()
    {
        $error = null;

        //Check if the variables are empty
        if ((empty($this->type)) || (empty($this->serial_nr) ) )
        {
            $error = "Please fill in the required (*)fields";
            return $error;
        }

        return true;
    }

}
?>
