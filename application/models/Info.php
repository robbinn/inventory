<?php

/**
 * Description of Info
 *
 * @author robert.gislason
 */
class Application_Model_Info extends Zend_Db_Table_Abstract{
    private $id;
    private $got_nr;
    private $inst_date;
    private $host_name;
    private $usage;
    private $inv_type;
    private $manufacturer;
    private $maconomy_asset_nr;
    private $note;
    private $password;

    function __construct($got_nr, $inst_date, $host_name, $usage, $inv_type, $manufacturer, $maconomy_asset_nr, $note, $password) {
        $this->got_nr = $got_nr;
        $this->inst_date = $inst_date;
        $this->host_name = $host_name;
        $this->usage = $usage;
        $this->inv_type = $inv_type;
        $this->manufacturer = $manufacturer;
        $this->maconomy_asset_nr = $maconomy_asset_nr;
        $this->note = $note;
        $this->password = $password;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getGot_nr() {
        return $this->got_nr;
    }

    public function setGot_nr($got_nr) {
        $this->got_nr = $got_nr;
    }

    public function getInst_date() {
        return $this->inst_date;
    }

    public function setInst_date($inst_date) {
        $this->inst_date = $inst_date;
    }

    public function getHost_name() {
        return $this->host_name;
    }

    public function setHost_name($host_name) {
        $this->host_name = $host_name;
    }

    public function getUsage() {
        return $this->usage;
    }

    public function setUsage($usage) {
        $this->usage = $usage;
    }

    public function getInv_type() {
        return $this->inv_type;
    }

    public function setInv_type($inv_type) {
        $this->inv_type = $inv_type;
    }

    public function getManufacturer() {
        return $this->manufacturer;
    }

    public function setManufactorer($manufacturer) {
        $this->manufacturer = $manufacturer;
    }

    public function getMaconomy_asset_nr() {
        return $this->maconomy_asset_nr;
    }

    public function setMaconomy_asset_nr($maconomy_asset_nr) {
        $this->maconomy_asset_nr = $maconomy_asset_nr;
    }

    public function getNote() {
        return $this->note;
    }

    public function setNote($note) {
        $this->note = $note;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function isValid()
    {
        $error = null;

        //Check if the variables are empty
        if ((empty($this->got_nr)) || (empty($this->maconomy_asset_nr) ) )
        {
            $error = "Please fill in the required (*)fields";
            return $error;
        }

        return true;
    }

}
?>
