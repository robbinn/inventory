<?php

/**
 * Description of Agreement
 *
 * @author robert.gislason
 */
class Application_Model_Logger extends Zend_Db_Table_Abstract
{

    private $id;
    private $ticket_nr;
    private $support_agreement;
    private $support_end;
    private $bought_from;
    private $bought_on;
    private $order_nr;
    private $got_nr;
    private $installation_date;
    private $hostname;
    private $usage;
    private $inv_type;
    private $manufacturer;
    private $maconomy;
    private $note;
    private $password;
    private $real_name;
    private $user_name;
    private $responsible;
    private $department;
    private $consultant;
    private $model;
    private $product_nr;
    private $type;
    private $serial_nr;
    private $cpu;
    private $disc;
    private $ram;
    private $graphics;
    private $os;
    private $active;
    private $onstock;
    private $datetime;
    private $action;

    
    function __construct(
            $ticket_nr, $support_agreement, $support_end, $bought_from, $bought_on,
            $order_nr, $got_nr, $installation_date, $hostname, $usage, $inv_type,
            $manufacturer, $maconomy, $note, $password, $real_name, $user_name,
            $responsible, $department, $consultant, $model, $product_nr, $type,
            $serial_nr, $cpu, $disc, $ram, $graphics, $os, $active, $onstock,
            $datetime, $action
            )
    {
        $this->ticket_nr = $ticket_nr;
        $this->support_agreement = $support_agreement;
        $this->support_end = $support_end;
        $this->bought_from = $bought_from;
        $this->bought_on = $bought_on;
        $this->order_nr = $order_nr;
        $this->got_nr = $got_nr;
        $this->installation_date = $installation_date;
        $this->hostname = $hostname;
        $this->usage = $usage;
        $this->inv_type = $inv_type;
        $this->manufacturer = $manufacturer;
        $this->maconomy = $maconomy;
        $this->note = $note;
        $this->password = $password;
        $this->real_name = $real_name;
        $this->user_name = $user_name;
        $this->responsible = $responsible;
        $this->department = $department;
        $this->consultant = $consultant;
        $this->model = $model;
        $this->product_nr = $product_nr;
        $this->type = $type;
        $this->serial_nr = $serial_nr;
        $this->cpu = $cpu;
        $this->disc = $disc;
        $this->ram = $ram;
        $this->graphics = $graphics;
        $this->os = $os;
        $this->active = $active;
        $this->onstock = $onstock;
        $this->datetime = $datetime;
        $this->action = $action;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getTicket_nr()
    {
        return $this->ticket_nr;
    }

    public function setTicket_nr($ticket_nr)
    {
        $this->ticket_nr = $ticket_nr;
    }

    public function getSupport_agreement()
    {
        return $this->support_agreement;
    }

    public function setSupport_agreement($support_agreement)
    {
        $this->support_agreement = $support_agreement;
    }

    public function getSupport_end()
    {
        return $this->support_end;
    }

    public function setSupport_end($support_end)
    {
        $this->support_end = $support_end;
    }

    public function getBought_from()
    {
        return $this->bought_from;
    }

    public function setBought_from($bought_from)
    {
        $this->bought_from = $bought_from;
    }

    public function getBought_on()
    {
        return $this->bought_on;
    }

    public function setBought_on($bought_on)
    {
        $this->bought_on = $bought_on;
    }

    public function getOrder_nr()
    {
        return $this->order_nr;
    }

    public function setOrder_nr($order_nr)
    {
        $this->order_nr = $order_nr;
    }

    public function getGot_nr()
    {
        return $this->got_nr;
    }

    public function setGot_nr($got_nr)
    {
        $this->got_nr = $got_nr;
    }

    public function getInstallation_date()
    {
        return $this->installation_date;
    }

    public function setInstallation_date($installation_date)
    {
        $this->installation_date = $installation_date;
    }

    public function getHostname()
    {
        return $this->hostname;
    }

    public function setHostname($hostname)
    {
        $this->hostname = $hostname;
    }

    public function getUsage()
    {
        return $this->usage;
    }

    public function setUsage($usage)
    {
        $this->usage = $usage;
    }

    public function getInv_type()
    {
        return $this->inv_type;
    }

    public function setInv_type($inv_type)
    {
        $this->inv_type = $inv_type;
    }

    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    public function getMaconomy()
    {
        return $this->maconomy;
    }

    public function setMaconomy($maconomy)
    {
        $this->maconomy = $maconomy;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function setNote($note)
    {
        $this->note = $note;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getReal_name()
    {
        return $this->real_name;
    }

    public function setReal_name($real_name)
    {
        $this->real_name = $real_name;
    }

    public function getUser_name()
    {
        return $this->user_name;
    }

    public function setUser_name($user_name)
    {
        $this->user_name = $user_name;
    }

    public function getResponsible()
    {
        return $this->responsible;
    }

    public function setResponsible($responsible)
    {
        $this->responsible = $responsible;
    }

    public function getDepartment()
    {
        return $this->department;
    }

    public function setDepartment($department)
    {
        $this->department = $department;
    }

    public function getConsultant()
    {
        return $this->consultant;
    }

    public function setConsultant($consultant)
    {
        $this->consultant = $consultant;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function getProduct_nr()
    {
        return $this->product_nr;
    }

    public function setProduct_nr($product_nr)
    {
        $this->product_nr = $product_nr;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getSerial_nr()
    {
        return $this->serial_nr;
    }

    public function setSerial_nr($serial_nr)
    {
        $this->serial_nr = $serial_nr;
    }

    public function getCpu()
    {
        return $this->cpu;
    }

    public function setCpu($cpu)
    {
        $this->cpu = $cpu;
    }

    public function getDisc()
    {
        return $this->disc;
    }

    public function setDisc($disc)
    {
        $this->disc = $disc;
    }

    public function getRam()
    {
        return $this->ram;
    }

    public function setRam($ram)
    {
        $this->ram = $ram;
    }

    public function getGraphics()
    {
        return $this->graphics;
    }

    public function setGraphics($graphics)
    {
        $this->graphics = $graphics;
    }

    public function getOs()
    {
        return $this->os;
    }

    public function setOs($os)
    {
        $this->os = $os;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = $active;
    }

    public function getOnstock()
    {
        return $this->onstock;
    }

    public function setOnstock($onstock)
    {
        $this->onstock = $onstock;
    }

    public function getDatetime()
    {
        return $this->datetime;
    }

    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setAction($action)
    {
        $this->action = $action;
    }

}

?>
