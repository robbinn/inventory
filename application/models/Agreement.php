<?php

/**
 * Description of Agreement
 *
 * @author robert.gislason
 */

class Application_Model_Agreement extends Zend_Db_Table_Abstract
{

    private $id;
    private $ticket_nr;
    private $support_agreement;
    private $support_end;
    private $bought_from;
    private $bought_on;
    private $order_nr;

    function __construct($ticket_nr, $support_agreement, $support_end, $bought_from, $bought_on, $order_nr)
    {
        $this->ticket_nr = $ticket_nr;
        $this->support_agreement = $support_agreement;
        $this->support_end = $support_end;
        $this->bought_from = $bought_from;
        $this->bought_on = $bought_on;
        $this->order_nr = $order_nr;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getTicket_nr()
    {
        return $this->ticket_nr;
    }

    public function setTicket_nr($ticket_nr)
    {
        $this->ticket_nr = $ticket_nr;
    }

    public function getSupport_agreement()
    {
        return $this->support_agreement;
    }

    public function setSupport_agreement($support_agreement)
    {
        $this->support_agreement = $support_agreement;
    }

    public function getSupport_end()
    {
        return $this->support_end;
    }

    public function setSupport_end($support_end)
    {
        $this->support_end = $support_end;
    }

    public function getBought_from()
    {
        return $this->bought_from;
    }

    public function setBought_from($bought_from)
    {
        $this->bought_from = $bought_from;
    }

    public function getBought_on()
    {
        return $this->bought_on;
    }

    public function setBought_on($bought_on)
    {
        $this->bought_on = $bought_on;
    }

    public function getOrder_nr()
    {
        return $this->order_nr;
    }

    public function setOrder_nr($order_nr)
    {
        $this->order_nr = $order_nr;
    }

    public function isValid()
    {
        $error = null;

        //Check if the variables are empty
        if ((empty($this->support_end) ))
        {
            $error = "Please fill in the required (*)fields";
            return $error;
        }
        
        //Check if support end is a date
        $date_array = explode("-", $this->support_end);
        $year = $date_array[0];
        $month = $date_array[1];
        $day = $date_array[2];
        
        if(!checkdate($month, $day, $year))
        {
            $error = "Support end date is not a valid date";
            return $error;
        }


        return true;
    }

}

?>
