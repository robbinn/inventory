<?php   
 // Standard inclusions      
 include("pChart/pData.class");   
 include("pChart/pChart.class");   
 /*
     Function that creates a chart image.
     Parameters: ValueArray, DateArray, LegendName, XAxisName, YAxisName, Title, Maxvalue
 */

function createChartImage($valuearray, $datearray, $legendname, $xaxisname, $yaxisname, $title, $maxvalue, $filename)
{
 // Dataset definition    
 $DataSet = new pData;
 $DataSet->AddPoint($valuearray,"Serie1");
 $DataSet->AddPoint($datearray,"Serie4");
 $DataSet->AddSerie("Serie1"); 

 $DataSet->SetAbsciseLabelSerie("Serie4");
 $DataSet->SetSerieName($legendname,"Serie1");
 $DataSet->SetYAxisName($yaxisname);
 $DataSet->SetXAxisName($xaxisname);
  
 // Initialise the graph   
 $Chart = new pChart(700,300);
 $Chart->setColorPalette(0,0,0,255);
 $Chart->reportWarnings("GD");

 $Chart->setFontProperties("Fonts/tahoma.ttf",8);   
 $Chart->setGraphArea(65,30,650,200); 
 
 $Chart->drawFilledRoundedRectangle(7,7,693,223,5,240,240,240);   
 $Chart->drawRoundedRectangle(5,5,695,225,5,230,230,230);   
 $Chart->drawGraphArea(255,255,255,TRUE);
 $Chart->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_NORMAL,150,150,150,TRUE,90,2,TRUE);   
 $Chart->drawGrid(4,TRUE,230,230,230,50);

 // Draw the 0 line   
 $Chart->setFontProperties("Fonts/tahoma.ttf",6);   
 $Chart->drawTreshold(0,143,55,72,TRUE,TRUE);   
  
 // Draw the area
 $Chart->drawLineGraph($DataSet->GetData(),$DataSet->GetDataDescription());   

 // Draw the line graph
 $Chart->setLineStyle(2,1);
 $DataSet->RemoveAllSeries();
 $DataSet->AddSerie("Serie1");
 $Chart->drawLineGraph($DataSet->GetData(),$DataSet->GetDataDescription());   
 $Chart->drawPlotGraph($DataSet->GetData(),$DataSet->GetDataDescription(),3,2,255,255,255);   

 // Write values on Serie3
 $Chart->setFontProperties("Fonts/tahoma.ttf",8);   
 $Chart->writeValues($DataSet->GetData(),$DataSet->GetDataDescription(),"Serie1");   
  
 // Finish the graph   
 $Chart->setFontProperties("Fonts/tahoma.ttf",8);   
 $Chart->drawLegend(590,90,$DataSet->GetDataDescription(),255,255,255);   
 $Chart->setFontProperties("Fonts/tahoma.ttf",10);   
 $Chart->drawTitle(60,22,$title,50,50,50,585);

 // Render the chart
 $Chart->Render($filename);   
 }
?>