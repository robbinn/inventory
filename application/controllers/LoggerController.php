<?php
session_start();

class LoggerController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->view->addHelperPath('ZendX/JQuery/View/Helper', 'ZendX_JQuery_View_Helper');
    }

    public function indexAction()
    {
        // action body
        $loggerService = new Application_Service_LoggerService();
        $allLogs = $loggerService->GetAllLogs();

        $this->view->logs = $allLogs;

    }

    public function logsbydateAction()
    {
        // action body
        $datetime = $this->_getParam('datetime');
        $loggerService = new Application_Service_LoggerService();
        $logsByDate = $loggerService->GetLogByDate($datetime);

        $this->view->logs = $logsByDate;
    }

    public function logsbygotnrAction()
    {
        $gotnr = $this->_getParam('gotnr');
        $loggerService = new Application_Service_LoggerService();
        $logsByGotnr = $loggerService->GetLogByGotnr($gotnr);

        $this->view->logs = $logsByGotnr;
    }


}

