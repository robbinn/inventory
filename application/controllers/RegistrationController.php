<?php

session_start();

class RegistrationController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->view->addHelperPath('ZendX/JQuery/View/Helper', 'ZendX_JQuery_View_Helper');

        /*
         * Testing if I get users from database
         */
        $userservice = new Application_Service_UserService();
        $_SESSION["allusernames"] = $userservice->GetAllUserNames();

        $deviceservice = new Application_Service_DeviceService();
        $_SESSION['alltypes'] = $deviceservice->GetAllTypes();

        $deviceservice = new Application_Service_DeviceService();
        $_SESSION['allmodels'] = $deviceservice->GetAllModels();
    }

    public function processnewdeviceAction()
    {
        if ($this->getRequest()->isPost())
        {
            $p_ticketnr = $this->_getParam('ticket_nr');
            $p_ordernr = $this->_getParam('order_nr');
            $p_gotnr = $this->_getParam('got_nr');
            $p_model =$this->_getParam('ac_model'); //Get one device id from this and get rest of common info
            $p_user = $this->_getParam('ac_user');
            

            if(!strcmp($p_model, "") == 0)
            {
                $deviceservice = new Application_Service_DeviceService();
                $device_info = $deviceservice->GetDeviceInfoByModel($p_model);
            }

            
            if(sizeof($device_info) > 0)
            {
                $_SESSION['type'] = $device_info[0]['type'];
                $_SESSION['inv_type'] = $device_info[0]['inv_type'];
                $_SESSION['manufacturer'] = $device_info[0]['manufacturer'];
                $_SESSION['model'] = $device_info[0]['model'];
                $_SESSION['product_nr'] = $device_info[0]['product_nr'];
                $_SESSION['cpu'] = $device_info[0]['cpu'];
                $_SESSION['disc'] = $device_info[0]['disc'];
                $_SESSION['ram'] = $device_info[0]['ram'];
                $_SESSION['graphics'] = $device_info[0]['graphics'];
                $_SESSION['os'] = $device_info[0]['os'];
            }
            //Get the user info from ac_user and register in session
            $userservice = new Application_Service_UserService();
            $user_info = $userservice->FindUser($p_user);
            $_SESSION["user_id"] = $user_info['id'];
            $_SESSION["real_name"] = $user_info['real_name'];
            $_SESSION["user_name"] = $user_info['user_name'];
            $_SESSION["responsible"] = $user_info['responsible'];
            $_SESSION["department"] = $user_info['department'];
            $_SESSION["consultant"] = $user_info['consultant'];

            //Register ticket_nr in session
            $_SESSION["ticket_nr"] = $p_ticketnr;

            //Register order_nr in session
            $_SESSION["order_nr"] = $p_ordernr;

            //Register got_nr in session
            $_SESSION["got_nr"] = $p_gotnr;

            //Register p_user in session
            $_SESSION["p_user"] = $p_user;

            //Register got_nr in session
            $_SESSION["p_model"] = $p_model;

            
            //Redirect to info page
            $this->_redirect('registration/verifyregistration');
        }
    }

    public function verifyregistrationAction()
    {
            //Check if the got_nr typed is the whole gotnr or just the number
            $got_len = strlen($_SESSION["got_nr"]);
            $got_isnum = is_numeric($_SESSION["got_nr"]);
            $hostname = $_SESSION["got_nr"];
            //If got_nr is 4 letter number or 5 letter number
            if ($got_len == 1 && $got_isnum == true)
            {
                $hostname = "GOT00000".$_SESSION["got_nr"];
                $_SESSION["got_nr"] = $hostname;
            } else if ($got_len == 2 && $got_isnum == true)
            {
                $hostname = "GOT0000".$_SESSION["got_nr"];
                $_SESSION["got_nr"] = $hostname;
            } else if ($got_len == 3 && $got_isnum == true)
            {
                $hostname = "GOT000".$_SESSION["got_nr"];
                $_SESSION["got_nr"] = $hostname;
            } else if ($got_len == 4 && $got_isnum == true)
            {
                $hostname = "GOT00".$_SESSION["got_nr"];
                $_SESSION["got_nr"] = $hostname;
            } else if ($got_len == 5 && $got_isnum == true)
            {
                $hostname = "GOT0".$_SESSION["got_nr"];
                $_SESSION["got_nr"] = $hostname;
            } else if ($got_len == 6 && $got_isnum == true)
            {
                $hostname = "GOT".$_SESSION["got_nr"];
                $_SESSION["got_nr"] = $hostname;
            }

            $this->view->hostname = $hostname;

    }

    //Register the device
    public function processnewregistrationAction()
    {
        $user_id = 0;
        $agreement_id = 0;
        $info_id = 0;

        if ($this->getRequest()->isPost())
        {
                //Insert agreement and get id back
                $post_agreement = new Application_Model_Agreement(
                            $this->_getParam('ticket_nr'),
                            $this->_getParam('support_agreement'),
                            $this->_getParam('support_end'),
                            $this->_getParam('bought_from'),
                            $this->_getParam('bought_on'),
                            $this->_getParam('order_nr')
            );
                $agreementservice = new Application_Service_AgreementService();
                $agreement_insert_id = $agreementservice->NewAgreement($post_agreement);

                //Insert info and get id back
                $post_info = new Application_Model_Info(
                            $this->_getParam('got_nr'),
                            $this->_getParam('installation_date'),
                            $this->_getParam('hostname'),
                            $this->_getParam('usage'),
                            $this->_getParam('inv_type'),
                            $this->_getParam('manufacturer'),
                            $this->_getParam('maconomy'),
                            $this->_getParam('note'),
                            $this->_getParam('password')
            );
                $infoservice = new Application_Service_InfoService();
                $info_insert_id = $infoservice->NewInfo($post_info);

                //Depending if the $_SESSION['user_id'] is set insert that Id or Insert the new user and get id back
                $user_insert_id = null;
                if (isset($_SESSION['user_id']))
                {
                    $user_insert_id = $_SESSION['user_id'];

                    //Create user model
                    $post_user = new Application_Model_User(
                            $this->_getParam('real_name'),
                            $this->_getParam('user_name'),
                            $this->_getParam('responsible'),
                            $this->_getParam('department'),
                            $this->_getParam('consultant')

            );
                    $post_user->setId($user_insert_id);
                    //Do update
                    $userservice = new Application_Service_UserService();
                    $userservice->UpdateUser($post_user);
                } else
                {
                    $userservice = new Application_Service_UserService();
                    //Create user model
                    $post_user = new Application_Model_User(
                            $this->_getParam('real_name'),
                            $this->_getParam('user_name'),
                            $this->_getParam('responsible'),
                            $this->_getParam('department'),
                            $this->_getParam('consultant')
                     );
                    $user_insert_id = $userservice->NewUser($post_user);
                }
                
                $post_device = new Application_Model_Device(
                                $user_insert_id,
                                $agreement_insert_id,
                                $info_insert_id,
                                $this->_getParam('model'),
                                $this->_getParam('product_nr'),
                                $this->_getParam('type'),
                                $this->_getParam('serial_nr'),
                                $this->_getParam('cpu'),
                                $this->_getParam('disc'),
                                $this->_getParam('ram'),
                                $this->_getParam('graphics'),
                                $this->_getParam('os'),
                                1, 0);

                //Register device object into db
                $deviceservice = new Application_Service_DeviceService();
                $device_insert_id = $deviceservice->NewDevice($post_device);

                /*
                 * Add to logger
                 */
                $datetime = date("Y-m-d H:i:s", time());
                $action = "New registration";
                $loggerService = new Application_Service_LoggerService();
                $logger = new Application_Model_Logger(
                            $this->_getParam('ticket_nr'),
                            $this->_getParam('support_agreement'),
                            $this->_getParam('support_end'),
                            $this->_getParam('bought_from'),
                            $this->_getParam('bought_on'),
                            $this->_getParam('order_nr'),
                            $this->_getParam('got_nr'),
                            $this->_getParam('installation_date'),
                            $this->_getParam('hostname'),
                            $this->_getParam('usage'),
                            $this->_getParam('inv_type'),
                            $this->_getParam('manufacturer'),
                            $this->_getParam('maconomy'),
                            $this->_getParam('note'),
                            $this->_getParam('password'),
                            $this->_getParam('real_name'),
                            $this->_getParam('user_name'),
                            $this->_getParam('responsible'),
                            $this->_getParam('department'),
                            $this->_getParam('consultant'),
                            $this->_getParam('model'),
                            $this->_getParam('product_nr'),
                            $this->_getParam('type'),
                            $this->_getParam('serial_nr'),
                            $this->_getParam('cpu'),
                            $this->_getParam('disc'),
                            $this->_getParam('ram'),
                            $this->_getParam('graphics'),
                            $this->_getParam('os'),
                            1,
                            0,
                            $datetime,
                            $action
                        );

                $loggerService->NewLog($logger);
                //------------------------------------------------


                // ADD New computer to Computer Replacement Plan if it is computer
				$inventory_type = $this->_getParam('inv_type');
                if((strcmp($inventory_type, 'Desktop') == 0) || (strcmp($inventory_type, 'Laptop') == 0))
                {
					$currentDate = date("Y-m-d");
					$duedate = strtotime(date("Y-m-d", strtotime($currentDate)) . " +2 week");
					$duedate = date("Ymd", $duedate);
					$ticket = $this->_getParam('ticket_nr');
					$gotnr = $this->_getParam('got_nr');
					$email = $this->_getParam('user_name');
					$os = $this->_getParam('os');

					$crp = new Application_Service_ComputerReplacementService($duedate, $ticket, $gotnr, $email, $os);
					$crp->addComputerToCRP();
                }
                //----------------------------------------------------------------

                //Delete every session variable
                session_destroy();

                //Redirect to info page
                $this->_redirect('../index');
            }

        

    }


    public function importdataAction()
    {
        //Import users into user table
        //$this->importuserAction();
        //Create services
        $agreementservice = new Application_Service_AgreementService();
        $infoservice = new Application_Service_InfoService();
        $userservice = new Application_Service_UserService();
        $deviceservice = new Application_Service_DeviceService();

        //1. Les skrá í streng.
        $row = 0;
        if (($handle = fopen("c:\Inventory.csv", "r")) !== FALSE)
        {

            //Open file to import data
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
            {
                $row++;

                //Get related data from the array
                $ticket_nr = $data[1];
                $support_agreement = $data[19];
                $support_end = $data[21];
                $bought_from = $data[27];
                $bought_on = $data[28];
                $order_nr = $data[29];
                $got_nr = $data[0]; //May need to strip GOT00
                $inst_date = $data[2];
                $host_name = $data[3];
                $usage = $data[9];
                $inv_type = $data[10];
                $manufacturer = $data[11];
                $maconomy_asset_nr = $data[30];
                $note = $data[34];
                $password = $data[36];
                $real_name = $data[5];
                $user_name = $data[6];
                $responsible = $data[7];
                $department = $data[8];
                $model = $data[12];
                $product_nr = $data[13];
                $type = $data[14];
                $serial_nr = $data[15];
                $cpu = $data[16];
                $disc = $data[17];
                $ram = $data[18];
                $graphics = $data[22];
                $os = $data[23];
                $active = 1;

                //Create agreement model
                $agreement = new Application_Model_Agreement($ticket_nr, $support_agreement, $support_end, $bought_from, $bought_on, $order_nr);
                //Create info model
                $info = new Application_Model_Info($got_nr, $inst_date, $host_name, $usage, $inv_type, $manufacturer, $maconomy_asset_nr, $note, $password);

                $user_result = $userservice->CheckIfUserExists($real_name, $user_name);

                /*
                 * TODO:  Get the ID of the user row
                 */

                $user_id = $user_result['id'];
                if (is_null($user_id))
                {
                    $user_id = 1; //1 is the id of the Not Set user
                }

                $agreement_id = $agreementservice->NewAgreement($agreement);
                $info_id = $infoservice->NewInfo($info);
                //Create device model
                $device = new Application_Model_Device($user_id, $agreement_id, $info_id, $model, $product_nr, $type, $serial_nr, $cpu, $disc, $ram, $graphics, $os, $active);
                //Insert device
                $deviceservice->NewDevice($device);
            }
        }
        fclose($handle);

        echo "<center><h1>" . $row . " DEVICES INSERTED</h1></center>";
    }

    public function importuserAction()
    {
        $row = 0;
        $userservice = new Application_Service_UserService();

        /*
         * Insert users by real_name
         */
        if (($handle = fopen("c:\Inventory.csv", "r")) !== FALSE)
        {

            //Go through once and insert user with both realname and username
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
            {
                $real_name = $data[5];
                $user_name = $data[6];
                $responsible = $data[7];
                $department = $data[8];
                $consultant = 0;

                $real_name = mb_convert_encoding($real_name, "UTF-8", "ISO-8859-1");
                echo $real_name . "<br>";
                //Check if the real_name is already in db
                $user_result = $userservice->CheckIfUserExists($real_name, $user_name);

                //First element into the array if array is empty
                if (is_null($user_result['id']))
                {
                    //Do not insert empty real_name or user_name
                    if ((!empty($real_name)) && (!empty($user_name)))
                    {
                        $user = new Application_Model_User($real_name, $user_name, $responsible, $department, $consultant);
                        $user_id = $userservice->NewUser($user); //Insert into db

                        $row++;
                    }
                }
            }
        }
        fclose($handle);


        echo "<center><h1>" . $row . " USERS INSERTED</H1>";
    }

    public function importremovedataAction()
    {
        //Import users into user table
        //Create services
        $agreementservice = new Application_Service_AgreementService();
        $infoservice = new Application_Service_InfoService();
        //$userservice = new Application_Service_UserService();
        $deviceservice = new Application_Service_DeviceService();

        //1. Les skrá í streng.
        $row = 0;
        if (($handle = fopen("c:\Inventory-removed.csv", "r")) !== FALSE)
        {

            //Open file to import data
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
            {
                $row++;

                //Get related data from the array
                $ticket_nr = "";
                $note1 = $data[1];
                $support_agreement = $data[17];
                $support_end = $data[19];
                $bought_from = $data[25];
                $bought_on = $data[26];
                $order_nr = $data[27];
                $got_nr = $data[0];
                $inst_date = $data[2];
                $host_name = $data[3];
                $usage = $data[7];
                $inv_type = $data[8];
                $manufacturer = $data[9];
                $maconomy_asset_nr = $data[28];
                $note = $note1 . " - " . $data[32];
                $user_name = $data[5];
                $responsible = $data[6];
                $model = $data[10];
                $product_nr = $data[11];
                $type = $data[12];
                $serial_nr = $data[13];
                $cpu = $data[14];
                $disc = $data[15];
                $ram = $data[16];
                $graphics = $data[20];
                $os = $data[21];
                $password = "";
                $active = 0;

                //Create agreement model
                $agreement = new Application_Model_Agreement($ticket_nr, $support_agreement, $support_end, $bought_from, $bought_on, $order_nr);
                //Create info model
                $info = new Application_Model_Info($got_nr, $inst_date, $host_name, $usage, $inv_type, $manufacturer, $maconomy_asset_nr, $note, $password);

                $user_id = 1;

                $agreement_id = $agreementservice->NewAgreement($agreement);
                $info_id = $infoservice->NewInfo($info);
                //Create device model
                $device = new Application_Model_Device($user_id, $agreement_id, $info_id, $model, $product_nr, $type, $serial_nr, $cpu, $disc, $ram, $graphics, $os, $active);
                //Insert device
                $deviceservice->NewDevice($device);
            }
        }
        fclose($handle);

        echo "<center><h1>" . $row . " DEVICES INSERTED</h1></center>";
    }

}

