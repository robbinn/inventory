<?php

include_once ("excel.php");
session_start();

/**
 * Description of ListsController
 *
 * @author robert.gislason
 */
class ListController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->view->addHelperPath('ZendX/JQuery/View/Helper', 'ZendX_JQuery_View_Helper');

        //Delete every session variable
        session_destroy();

        /*
         * Testing if I get users from database
         */
        $userservice = new Application_Service_UserService();
        $_SESSION["allusernames"] = $userservice->GetAllUserNames();
    }

    public function activedeviceAction()
    {
        $page = $this->_getParam('page');
        //Get list array with all devices and display it
        $deviceservice = new Application_Service_DeviceService();
        $devicelist = $deviceservice->GetActiveDevices();

        //Create excel sheet. Send in list and name for file
        $this->createExcel($devicelist, "active_devices");

        // Get a Paginator object using Zend_Paginator's built-in factory.
        $paginator = Zend_Paginator::factory($devicelist);

        // Select the second page
        $paginator->setCurrentPageNumber($page, 1);
        $paginator->setDefaultItemCountPerPage(100);

        $this->view->total = sizeof($devicelist);
        $this->view->currentpage = $page;
        $this->view->list = $paginator;
    }

    public function inactivedeviceAction()
    {
        $page = $this->_getParam('page');
        //Get list array with all devices and display it
        $deviceservice = new Application_Service_DeviceService();
        $devicelist = $deviceservice->GetInActiveDevices();

        //Create excel sheet. Send in list and name for file
        $this->createExcel($devicelist, "removed_devices");

        // Get a Paginator object using Zend_Paginator's built-in factory.
        $paginator = Zend_Paginator::factory($devicelist);

        // Select the second page
        $paginator->setCurrentPageNumber($page, 1);
        $paginator->setDefaultItemCountPerPage(100);

        $this->view->total = sizeof($devicelist);
        $this->view->currentpage = $page;
        $this->view->list = $paginator;
    }

    public function supportendingAction()
    {
        $page = $this->_getParam('page');

        $deviceservice = new Application_Service_DeviceService();

        $devices = $deviceservice->getSupportEndDevices();

        //Create excel sheet. Send in list and name for file
        $this->createExcel($devices, "support_ending");
        
        // Get a Paginator object using Zend_Paginator's built-in factory.
        $paginator = Zend_Paginator::factory($devices);

        // Select the second page
        $paginator->setCurrentPageNumber($page, 1);
        $paginator->setDefaultItemCountPerPage(10);

        $this->view->total = sizeof($devices);
        $this->view->currentpage = $page;
        $this->view->list = $paginator;
    }

    public function GetDays($sStartDate, $sEndDate)
    {
        // Firstly, format the provided dates.
        // This function works best with YYYY-MM-DD
        // but other date formats will work thanks
        // to strtotime().
        $sStartDate = gmdate("Y-m-d", strtotime($sStartDate));
        $sEndDate = gmdate("Y-m-d", strtotime($sEndDate));

        // Start the variable off with the start date
        $aDays[] = $sStartDate;

        // Set a 'temp' variable, sCurrentDate, with
        // the start date - before beginning the loop
        $sCurrentDate = $sStartDate;

        // While the current date is less than the end date
        while ($sCurrentDate < $sEndDate)
        {
            // Add a day to the current date
            $sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));

            // Add this new day to the aDays array
            $aDays[] = $sCurrentDate;
        }

        // Once the loop has finished, return the
        // array of days.
        return $aDays;
    }

    public function viewitemAction()
    {
        //Get the id from the request
        $id = null;
        if ($this->getRequest()->isGet())
        {
            $id = $this->_getParam('id');
        }

        //Get list array with all devices and display it
        $deviceservice = new Application_Service_DeviceService();
        $devicelist = $deviceservice->GetDeviceById($id);
        $userlist = null;
        $agreementlist = null;
        $infolist = null;

        //Create services to get related data for the device
        $userservice = new Application_Service_UserService();
        $infoservice = new Application_Service_InfoService();
        $agreementservice = new Application_Service_AgreementService();


        for ($i = 0; $i < sizeof($devicelist); $i++)
        {
            //Here we need to fetch all info from the result sets
            //User
            $user = $userservice->getUserById($devicelist['user_id']);
            $userlist[$i]['id'] = $user['id'];
            $userlist[$i]['real_name'] = $user['real_name'];
            $userlist[$i]['user_name'] = $user['user_name'];
            $userlist[$i]['responsible'] = $user['responsible'];
            $userlist[$i]['department'] = $user['department'];
            $userlist[$i]['consultant'] = $user['consultant'];

            //Info
            $info = $infoservice->GetInfoById($devicelist['info_id']);
            $infolist[$i]['id'] = $info['id'];
            $infolist[$i]['got_nr'] = $info['got_nr'];
            $infolist[$i]['inst_date'] = $info['inst_date'];
            $infolist[$i]['host_name'] = $info['host_name'];
            $infolist[$i]['usage'] = $info['usage'];
            $infolist[$i]['inv_type'] = $info['inv_type'];
            $infolist[$i]['manufacturer'] = $info['manufacturer'];
            $infolist[$i]['maconomy_asset_nr'] = $info['maconomy_asset_nr'];
            $infolist[$i]['note'] = $info['note'];
            $infolist[$i]['password'] = $info['password'];

            //Agreement
            $agreement = $agreementservice->GetAgreementById($devicelist['agreement_id']);
            $agreementlist[$i]['id'] = $agreement['id'];
            $agreementlist[$i]['ticket_nr'] = $agreement['ticket_nr'];
            $agreementlist[$i]['support_agreement'] = $agreement['support_agreement'];
            $agreementlist[$i]['support_end'] = $agreement['support_end'];
            $agreementlist[$i]['bought_from'] = $agreement['bought_from'];
            $agreementlist[$i]['bought_on'] = $agreement['bought_on'];
            $agreementlist[$i]['order_nr'] = $agreement['order_nr'];
        }

        //Get the user devices
        $otherdevices = $deviceservice->GetUserDevices($devicelist['user_id']);

        $this->view->list = $devicelist;
        $this->view->user = $userlist;
        $this->view->agreement = $agreementlist;
        $this->view->info = $infolist;
        $this->view->otherdevices = $otherdevices;
    }

    public function setdeviceonstockAction()
    {
        $frompage = $this->_getParam('page');
        $set_on_stock = $this->_getParam('btn_set_on_stock');

        /**
         * Update the user
         */
        //1. Set the real name of the user which is ITS if on stock
        $new_name = "ITS";

        //2. Find this user in the db by using user service controller
        $user_service_ctrl = new Application_Service_UserService();
        $user = $user_service_ctrl->FindUser($new_name);

        //2.1 Find the device by id that is related to this user
        $device_service_ctrl = new Application_Service_DeviceService();
        $device_id = $this->_getParam('device_id1');

        $device = $device_service_ctrl->GetDeviceById($device_id);
        //3. Get the new user id in a variable
        $new_user_id = $user['id'];

        //4. Populate device model with all info and the new user_id
        $device_model = new Application_Model_Device($new_user_id,
                        $device['agreement_id'],
                        $device['info_id'],
                        $device['model'],
                        $device['product_nr'],
                        $device['type'],
                        $device['serial_nr'],
                        $device['cpu'],
                        $device['disc'],
                        $device['ram'],
                        $device['graphics'],
                        $device['os'],
                        $device['active'], 1);

        $device_model->setId($device_id);
        //6. Update the table using device service controller
        $device_service_ctrl->UpdateDevice($device_model);

        //LOG the action update user----------------------------------------
                //Get gotnr
                $infoservice = new Application_Service_InfoService();
                $info = $infoservice->GetInfoById($device['info_id']);
                $datetime = date("Y-m-d H:i:s", time());
                $action = "Device set on stock";
                $loggerService = new Application_Service_LoggerService();
                $logger = new Application_Model_Logger("", "", "", "", "", "", $info['got_nr'], "", "", "",
                            "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                            "", "", "", "", "", 1, $datetime, $action);
                $loggerService->NewLog($logger);
            //--------------------------------------------------------

        //7. Redirect to viewitem using the device id as parameter
        $page = 'list/viewitem?id=' . $device_id;
        $this->_redirect($page);
    }

    public function updateitemAction()
    {
        $device_info = $this->_getParam('btn_update');
        $update_user = $this->_getParam('btn_update_user');
        $frompage = $this->_getParam('page');
        $change_user = $this->_getParam('btn_change_user');

        /**
         * Update the user
         */
        if (isset($change_user))
        {

            //1. Get the real name of the user
            $new_name = $this->_getParam('real_name');

            //2. Find this user in the db by using user service controller
            $user_service_ctrl = new Application_Service_UserService();
            $user = $user_service_ctrl->FindUser($new_name);

            //2.1 Find the device by id that is related to this user
            $device_service_ctrl = new Application_Service_DeviceService();
            $device_id = $this->_getParam('device_id2');
            $device = $device_service_ctrl->GetDeviceById($device_id);

            $new_user_id = null;
            //If user does not exist he must be created
            if ($user == null)
            {
                //Populate a user object with user params
                $real_name = $this->_getParam('real_name');
                $user_name = $this->_getParam('user_name');
                $responsible = $this->_getParam('responsible');
                $department = $this->_getParam('department');
                $consultant = $this->_getParam('consultant');

                $user_model = new Application_Model_User($real_name, $user_name, $responsible, $department, $consultant);

                //Insert into user table
                $new_user_id = $user_service_ctrl->NewUser($user_model);
            } else
            {
                //3. Get the new user id in a variable
                $new_user_id = $user['id'];
            }




            //4. Populate device model with all info and the new user_id
            $device_model = new Application_Model_Device($new_user_id,
                            $device['agreement_id'],
                            $device['info_id'],
                            $device['model'],
                            $device['product_nr'],
                            $device['type'],
                            $device['serial_nr'],
                            $device['cpu'],
                            $device['disc'],
                            $device['ram'],
                            $device['graphics'],
                            $device['os'],
                            $device['active'],
                            0);

            $device_model->setId($device_id);
            //6. Update the table using device service controller
            $device_service_ctrl->UpdateDevice($device_model);

            //LOG the action change user for device-----------------------------
                //Get gotnr for device
                $infoservice = new Application_Service_InfoService();
                $infoitem = $infoservice->GetInfoById($device['info_id']);
                $datetime = date("Y-m-d H:i:s", time());
                $action = "Change user for device";
                $loggerService = new Application_Service_LoggerService();
                $logger = new Application_Model_Logger("", "", "", "", "", "", $infoitem['got_nr'], "", "", "",
                            "", "", "", "", "", $new_name, "", "", "", "", "", "", "", "", "",
                            "", "", "", "", "", "", $datetime, $action);
                $logger->setDatetime($datetime);
                $logger->setAction($action);
                $logger->setReal_name($new_name);
                $loggerService->NewLog($logger);
            //--------------------------------------------------------

            //7. Redirect to viewitem using the device id as parameter
            $page = 'list/viewitem?id=' . $device_id;
            $this->_redirect($page);
        } else if (isset($update_user))
        {
            $user_service_ctrl = new Application_Service_UserService();
            //Populate a user object with user params
            $real_name = $this->_getParam('real_name');
            $user_name = $this->_getParam('user_name');
            $responsible = $this->_getParam('responsible');
            $department = $this->_getParam('department');
            $consultant = $this->_getParam('consultant');

            if($consultant == '1')
            {
                $consultant = 1;
            }
            else
            {
                $consultant = 0;
            }

            $user_model = new Application_Model_User($real_name, $user_name, $responsible, $department, $consultant);
            $user_model->setId($this->_getParam('user_id'));
            $user_id = $this->_getParam('user_id');
            //Update user only if it is not ITS or Not set user
            if(($user_id == "18205") || ($user_id == "1"))
            {
                $device_id = $this->_getParam('device_id2');
                $page = 'list/viewitem?id=' . $device_id.'&error=You can not change this user';
                $this->_redirect($page);
            }
            else
            {
                $user_service_ctrl->UpdateUser($user_model);
            }
            //LOG the action update user----------------------------------------
                $datetime = date("Y-m-d H:i:s", time());
                $action = "Update user information";
                $loggerService = new Application_Service_LoggerService();
                $logger = new Application_Model_Logger("", "", "", "", "", "", "", "", "", "",
                            "", "", "", "", "", $real_name, $user_name, $responsible, $department, $consultant, "", "", "", "", "",
                            "", "", "", "", "", "", $datetime, $action);
                $loggerService->NewLog($logger);
            //--------------------------------------------------------

            $device_id = $this->_getParam('device_id2');

            $page = "";
            if (isset($frompage))
            {
                $page = "list/users";
            } else
            {
                $page = 'list/viewitem?id=' . $device_id;
            }
            $this->_redirect($page);
        } else if (isset($device_info))
        {
            /**
             * Update the device, info or agreement
             */
            $device_id1 = $this->_getParam('device_id1');
            $user_id = $this->_getParam('user_id');
            $agreement_id = $this->_getParam('agreement_id');
            $info_id = $this->_getParam('info_id');
            $model = $this->_getParam('model');
            $product_nr = $this->_getParam('product_nr');
            $type = $this->_getParam('type');
            $serial_nr = $this->_getParam('serial_nr');
            $cpu = $this->_getParam('cpu');
            $disc = $this->_getParam('disc');
            $ram = $this->_getParam('ram');
            $graphics = $this->_getParam('graphics');
            $os = $this->_getParam('os');
            $active = $this->_getParam('active');
            $onstock = $this->_getParam('onstock');

            $got_nr = $this->_getParam('got_nr');
            $inst_date = $this->_getParam('inst_date');
            $host_name = $this->_getParam('host_name');
            $usage = $this->_getParam('usage');
            $inv_type = $this->_getParam('inv_type');
            $manufacturer = $this->_getParam('manufacturer');
            $maconomy_asset_nr = $this->_getParam('maconomy_asset_nr');
            $note = $this->_getParam('note');
            $password = $this->_getParam('password');

            $ticket_nr = $this->_getParam('ticket_nr');
            $support_agreement = $this->_getParam('support_agreement');
            $support_end = $this->_getParam('support_end');
            $bought_from = $this->_getParam('bought_from');
            $bought_on = $this->_getParam('bought_on');
            $order_nr = $this->_getParam('order_nr');

            //Populate model objects with getParam info
            $device_model = new Application_Model_Device($user_id, $agreement_id, $info_id, $model, $product_nr, $type, $serial_nr, $cpu, $disc, $ram, $graphics, $os, $active, $onstock);
            $info_model = new Application_Model_Info($got_nr, $inst_date, $host_name, $usage, $inv_type, $manufacturer, $maconomy_asset_nr, $note, $password);
            $agreement_model = new Application_Model_Agreement($ticket_nr, $support_agreement, $support_end, $bought_from, $bought_on, $order_nr);

            //Set id on model objects
            $device_model->setId($device_id1);
            $info_model->setId($info_id);
            $agreement_model->setId($agreement_id);

            //Create service controllers
            $device_service_ctrl2 = new Application_Service_DeviceService();
            $agreement_service_ctrl = new Application_Service_AgreementService();
            $info_service_ctrl = new Application_Service_InfoService();

            //Call update function and send in the objects
            $device_service_ctrl2->UpdateDevice($device_model);
            $info_service_ctrl->UpdateInfo($info_model);
            $agreement_service_ctrl->UpdateAgreement($agreement_model);

            //LOG the action update device----------------------------------------
                //Get the user
                $userservice = new Application_Service_UserService();
                $userinfo = $userservice->getUserById($user_id);
                $datetime = date("Y-m-d H:i:s", time());
                $action = "Update device information";
                $loggerService = new Application_Service_LoggerService();
                $logger = new Application_Model_Logger($ticket_nr, $support_agreement, $support_end, $bought_from, $bought_on, $order_nr, $got_nr, $inst_date, $host_name, $usage,
                            $inv_type, $manufacturer, $maconomy_asset_nr, $note, $password, $userinfo['real_name'], $userinfo['user_name'], $userinfo['responsible'], $userinfo['department'], $userinfo['consultant'], $model, $product_nr, $type, $serial_nr, $cpu,
                            $disc, $ram, $graphics, $os, $active, $onstock, $datetime, $action);
                $loggerService->NewLog($logger);
            //--------------------------------------------------------

            //Redirect the page.
            $page = 'list/viewitem?id=' . $device_id1;
            $this->_redirect($page);
        }
    }

    public function finddeviceAction()
    {
        $cond = $this->_getParam('searchstring');

        $device_service_ctrl = new Application_Service_DeviceService();
        $results = $device_service_ctrl->search($cond);
        $this->view->result = $results;
        $this->view->cond = $cond;
    }

    public function finduserAction()
    {
        $cond = $this->_getParam('searchstring');

        $user_service_ctrl = new Application_Service_UserService();
        $results = $user_service_ctrl->SearchForUser($cond);
        $this->view->result = $results;
        $this->view->cond = $cond;
    }

    public function onstockAction()
    {
        $page = $this->_getParam('page');

        $deviceservice = new Application_Service_DeviceService();

        $devices = $deviceservice->getDevicesOnStock();

        //Create excel sheet. Send in list and name for file
        $this->createExcel($devices, "on_stock");

        // Get a Paginator object using Zend_Paginator's built-in factory.
        $paginator = Zend_Paginator::factory($devices);

        // Select the second page
        $paginator->setCurrentPageNumber($page, 1);
        $paginator->setDefaultItemCountPerPage(10);

        $this->view->total = sizeof($devices);
        $this->view->currentpage = $page;
        $this->view->list = $paginator;
    }

    public function monitorsAction()
    {

        $page = $this->_getParam('page');
        //Get list array with all devices and display it
        $deviceservice = new Application_Service_DeviceService();
        $devices = $deviceservice->getActiveMonitors();

        //Create excel sheet. Send in list and name for file
        $this->createExcel($devices, "monitors");

        // Get a Paginator object using Zend_Paginator's built-in factory.
        $paginator = Zend_Paginator::factory($devices);

        // Select the second page
        $paginator->setCurrentPageNumber($page, 1);
        $paginator->setDefaultItemCountPerPage(100);

        $this->view->total = sizeof($devices);
        $this->view->currentpage = $page;
        $this->view->list = $paginator;
    }

    public function laptopsAction()
    {

        $page = $this->_getParam('page');
        //Get list array with all devices and display it
        $deviceservice = new Application_Service_DeviceService();
        $devices = $deviceservice->getActiveLaptops();

        //Create excel sheet. Send in list and name for file
        $this->createExcel($devices, "laptops");

        // Get a Paginator object using Zend_Paginator's built-in factory.
        $paginator = Zend_Paginator::factory($devices);

        // Select the second page
        $paginator->setCurrentPageNumber($page, 1);
        $paginator->setDefaultItemCountPerPage(40);

        $this->view->total = sizeof($devices);
        $this->view->currentpage = $page;
        $this->view->list = $paginator;
    }

    public function desktopsAction()
    {

        $page = $this->_getParam('page');
        //Get list array with all devices and display it
        $deviceservice = new Application_Service_DeviceService();
        $devices = $deviceservice->getActiveDesktops();

        //Create excel sheet. Send in list and name for file
        $this->createExcel($devices, "desktops");

        // Get a Paginator object using Zend_Paginator's built-in factory.
        $paginator = Zend_Paginator::factory($devices);

        // Select the second page
        $paginator->setCurrentPageNumber($page, 1);
        $paginator->setDefaultItemCountPerPage(20);

        $this->view->total = sizeof($devices);
        $this->view->currentpage = $page;
        $this->view->list = $paginator;
    }

    public function usersAction()
    {
        $page = $this->_getParam('page');
        //Get list array with all devices and display it
        $userservice = new Application_Service_UserService();
        $userlist = $userservice->GetAllUsers();

        //Create excel sheet. Send in list and name for file
        $this->createExcel($devices, "users");

        // Get a Paginator object using Zend_Paginator's built-in factory.
        $paginator = Zend_Paginator::factory($userlist);

        // Select the second page
        $paginator->setCurrentPageNumber($page, 1);
        $paginator->setDefaultItemCountPerPage(50);

        $this->view->total = sizeof($userlist);
        $this->view->currentpage = $page;
        $this->view->list = $paginator;
    }

    public function users2Action()
    {

        //Get list array with all devices and display it
        $userservice = new Application_Service_UserService();
        $userlist = $userservice->GetAllUsers();

        $this->view->user = $userlist;
    }

    public function viewuserAction()
    {
        $id = $this->_getParam('id');
        //Get list array with all devices and display it
        $userservice = new Application_Service_UserService();
        $user = $userservice->getUserById($id);

        //Get the user devices
        $deviceservice = new Application_Service_DeviceService();
        $devices = $deviceservice->GetUserDevices($id);

        $this->view->user = $user;
        $this->view->devices = $devices;
    }

    public function createExcel($list, $filename)
    {
        /*
         * Create excel sheet from result set
         */
        $export_file = "xlsfile://workspace/inventory/public/".$filename.".xls";
        $fp = fopen($export_file, "wb");
        if (!is_resource($fp))
        {
            die("Cannot open $export_file");
        }
        $assoc = null;
        $i = 0;
        foreach ($list as $item)
        {
            //Create an excel file --------------------------------
            // typically this will be generated/read from a database table
            $assoc[$i] = array("Ticket nr" => $item['ticket_nr'],
                "Host name" => $item['host_name'],
                "Inv type" => $item['inv_type'],
                "Model" => $item['model'],
                "Type" => $item['type'],
                "Serial nr" => $item['serial_nr'],
                "Support end" => $item['support_end'],
                "Real name" => $item['real_name']);
            //-----------------------------------------------------
            $i++;
        }
        //close file
        fwrite($fp, serialize($assoc));
        fclose($fp);
    }
}

?>
