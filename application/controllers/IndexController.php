<?php

include("fnCreateChart.php");

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->view->addHelperPath('ZendX/JQuery/View/Helper', 'ZendX_JQuery_View_Helper');

        //Delete every session variable
        session_destroy();
    }

    public function indexAction()
    {

        $this->view->devicecount = $this->getTotalDeviceCount();
        $this->view->activedevicecount = $this->getTotalActiveDeviceCount();
        $this->view->activemonitorscount = $this->getTotalActiveMonitorsCount();
        $this->view->activelaptopscount = $this->getTotalActiveLaptopsCount();
        $this->view->activedesktopscount = $this->getTotalActiveDesktopsCount();
        $this->view->supportendcount = $this->getSupportEndCount();
        $this->view->usercount = $this->getUserCount();
        $this->view->onstockcount = $this->getOnStockCount();

        $monitors = $this->getTotalActiveMonitorsCount();
        $laptops = $this->getTotalActiveLaptopsCount();
        $desktops = $this->getTotalActiveDesktopsCount();
        //Create the image
        /* Create Weight chart */
        //Send date_array with value array along with Title and legend and X and Y titles
        $wlegendname = "Devices";
        $wxaxisname = "";
        $wyaxisname = "";
        $wtitle = "ITS Devices";
        $wfilename = "chart.png";
        $value_array = array($monitors, $laptops, $desktops);
        $legend_array = array("Monitors", "Laptops", "Desktops");
        $wmax = 100;

        if ($value_array != null && $legend_array != null)
        {
            createChartImage($value_array, $legend_array, $wlegendname, $wxaxisname, $wyaxisname, $wtitle, $wmax, $wfilename);
            //Display Chart image
            
        } else
        {
            echo "<center>Please enter data for chart</center>";
        }

        $this->view->last10inserts = $this->getLast10Inserts();
    }

    public function getTotalDeviceCount()
    {
        $device_service_ctrl = new Application_Service_DeviceService();
        $count = $device_service_ctrl->getDeviceCount();

        return $count;
    }

    public function getTotalActiveDeviceCount()
    {
        $device_service_ctrl = new Application_Service_DeviceService();
        $count = $device_service_ctrl->getActiveDeviceCount();

        return $count;
    }

    public function getTotalActiveMonitorsCount()
    {
        $device_service_ctrl = new Application_Service_DeviceService();
        $count = $device_service_ctrl->getActiveMonitorsCount();

        return $count;
    }

    public function getTotalActiveLaptopsCount()
    {
        $device_service_ctrl = new Application_Service_DeviceService();
        $count = $device_service_ctrl->getActiveLaptopsCount();

        return $count;
    }

    public function getTotalActiveDesktopsCount()
    {
        $device_service_ctrl = new Application_Service_DeviceService();
        $count = $device_service_ctrl->getActiveDesktopsCount();

        return $count;
    }

    public function getSupportEndCount()
    {
        $device_service_ctrl = new Application_Service_DeviceService();
        $count = $device_service_ctrl->getSupportEndCount();

        return $count;
    }

    public function getUserCount()
    {
        $user_service_ctrl = new Application_Service_UserService();
        $count = $user_service_ctrl->getUserCount();

        return $count;
    }

    public function getOnStockCount()
    {
        $device_service_ctrl = new Application_Service_DeviceService();
        $count = $device_service_ctrl->getOnStockCount();

        return $count;
    }

    public function getLast10Inserts()
    {
        $device_service_ctrl = new Application_Service_DeviceService();
        $last10 = $device_service_ctrl->GetLast10Inserts();

        return $last10;
    }

}

