<?php

/**
 * Description of ComputerReplacementService
 *
 * @author robert.gislason
 */
class Application_Service_ComputerReplacementService
{

    private $duedate;
    private $ticket;
    private $gotnr;
    private $email;
    private $os;
    private $oldcomputer;
    private $status;
    private $info;

    function __construct($duedate, $ticket, $gotnr, $email, $os)
    {
        $this->duedate = $duedate;
        $this->ticket = $ticket;
        $this->gotnr = $gotnr;
        $this->email = $email . "@jeppesen.com";
        $this->os = $os;
        $this->oldcomputer = "";
        $this->status = 0;
        $this->info = "";
    }

    function addComputerToCRP()
    {
        /*
         * Lesa xml og skrifa út aftur.
         */
        $xmlStr = '<?xml version="1.0" encoding="UTF-8"?><computers>';

        $objDOM = new DOMDocument();
        $objDOM->load("C:\wamp\www\crp\computers.xml"); //make sure path is correct


        $entries = $objDOM->getElementsByTagName("computer");
        // for each note tag, parse the document and get values for
        // tasks and details tag.

        $new_info = mb_convert_encoding($this->info, "UTF-8", "ISO-8859-1");

        //New entry
        $xmlStr = $xmlStr . '<computer><duedate>' . $this->duedate . '</duedate><ticket>' . $this->ticket . '</ticket><gotnr>' . $this->gotnr . '</gotnr><email>' . $this->email . '</email><os>' . $this->os . '</os><oldcomputer>' . $this->oldcomputer . '</oldcomputer><info>' . $new_info . '</info><status>' . $this->status . '</status></computer>';


        $i = 1;
        foreach ($entries as $value)
        {

            $value_date = $value->getElementsByTagName("duedate");
            $odate = $value_date->item(0)->nodeValue;

            $ticket_value = $value->getElementsByTagName("ticket");
            $oticket = $ticket_value->item(0)->nodeValue;

            $value_gotnr = $value->getElementsByTagName("gotnr");
            $ogotnr = $value_gotnr->item(0)->nodeValue;

            $value_email = $value->getElementsByTagName("email");
            $oemail = $value_email->item(0)->nodeValue;

            $value_os = $value->getElementsByTagName("os");
            $oos = $value_os->item(0)->nodeValue;

            $value_oldcomputer = $value->getElementsByTagName("oldcomputer");
            $ooldcomputer = $value_oldcomputer->item(0)->nodeValue;

            $value_gotnr = $value->getElementsByTagName("info");
            $oinfo = $value_gotnr->item(0)->nodeValue;

            $value_status = $value->getElementsByTagName("status");
            $ostatus = $value_status->item(0)->nodeValue;

            $value_returned = $value->getElementsByTagName("returned");
            $oreturned = $value_returned->item(0)->nodeValue;

            $value_scratched = $value->getElementsByTagName("scratched");
            $oscratched = $value_scratched->item(0)->nodeValue;

            $value_hidden = $value->getElementsByTagName("hidden");
            $ohidden = $value_hidden->item(0)->nodeValue;

            $xmlStr = $xmlStr . '<computer><duedate>' . $odate . '</duedate><ticket>' . $oticket . '</ticket><gotnr>' . $ogotnr . '</gotnr><email>' . $oemail . '</email><os>' . $oos . '</os><oldcomputer>' . $ooldcomputer . '</oldcomputer><info>' . $oinfo . '</info><status>' . $ostatus . '</status></computer>';
        }

        $xmlStr = $xmlStr . '</computers>';

        /*
         * Write new direcories.xml
         */

        $xmlfile = "C:\wamp\www\crp\computers.xml";
        $file = fopen($xmlfile, "w");
        fwrite($file, $xmlStr);
        fclose($file);
    }

}

?>
