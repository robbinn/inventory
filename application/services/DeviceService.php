<?php

/**
 * Description of UserService
 *
 * @author robert.gislason
 */
class Application_Service_DeviceService {

    protected $db;
    protected $device;

    function __construct() {
        $this->device = new Application_Model_DeviceTable();
    }

    public function NewDevice(Application_Model_Device $device) {
        $user_id = $device->getUser_id();
        $agreement_id = $device->getAgreement_id();
        $info_id = $device->getInfo_id();
        $model = $device->getModel();
        $product_nr = $device->getProduct_nr();
        $type = $device->getType();
        $serial_nr = $device->getSerial_nr();
        $cpu = $device->getCpu();
        $disc = $device->getDisc();
        $ram = $device->getRam();
        $graphics = $device->getGraphics();
        $os = $device->getOs();
        $active = $device->getActive();
        $onstock = $device->getOnstock();


        $params = array(
            'user_id' => $user_id,
            'agreement_id' => $agreement_id,
            'info_id' => $info_id,
            'model' => $model,
            'product_nr' => $product_nr,
            'type' => $type,
            'serial_nr' => $serial_nr,
            'cpu' => $cpu,
            'disc' => $disc,
            'ram' => $ram,
            'graphics' => $graphics,
            'os' => $os,
            'active' => $active,
            'onstock' => $onstock
        );
        return $this->device->insert($params);
    }

    /*
     * Find user in database and return user if exists
     * Parameters: User
     * Returns: User if found otherwise null
     */

    public function GetAllDevices() {
        $select = $this->device->select();
        $select->order('type');
        return $this->device->fetchAll($select);
    }

    public function GetAllTypes() {
        $select = $this->device->select()
                        ->from('device', array('type'));
        return $this->device->fetchAll($select);
    }

    public function GetAllModels() {
        $select = $this->device->select()->distinct()
                        ->from('device', array('model'));
        return $this->device->fetchAll($select);
    }

    public function GetAllDeviceSerialnr() {
        $select = $this->device->select()
                        ->from('device', array('serial_nr'));
        return $this->device->fetchAll($select);
    }

    public function FindDeviceBySerialnr($serial_nr) {
        $select = $this->device->select()
                        ->from('device')
                        ->where('serial_nr = ?', $serial_nr);

        return $this->device->fetchRow($select);
    }

    public function GetDeviceById($id) {
        $select = $this->device->select()
                        ->from('device')
                        ->where('id = ?', $id);

        return $this->device->fetchRow($select);
    }

    public function GetActiveDevices() {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('device.id', 'i.got_nr', 'a.ticket_nr', 'i.host_name', 'i.inv_type', 'device.model', 'device.type', 'device.serial_nr', 'a.support_end', 'u.real_name', 'device.user_id', 'u.consultant'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array())
                        ->join(array('u' => 'user'),
                                'device.user_id = u.id', array())
                        ->join(array('a' => 'agreement'),
                                'device.agreement_id = a.id', array())
                        ->where('device.active = 1')
                        ->order('i.got_nr DESC');
        
        $rows = $this->device->fetchAll($select);

        return $rows;
    }

    public function GetInactiveDevices() {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('device.id', 'i.got_nr', 'a.ticket_nr', 'i.host_name', 'i.inv_type', 'device.model', 'device.type', 'device.serial_nr', 'a.support_end', 'u.real_name'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array())
                        ->join(array('u' => 'user'),
                                'device.user_id = u.id', array())
                        ->join(array('a' => 'agreement'),
                                'device.agreement_id = a.id', array())
                        ->where('device.active = 0')
                        ->order('i.got_nr DESC');

        $rows = $this->device->fetchAll($select);

        return $rows;
    }

    public function UpdateDevice(Application_Model_Device $device) {
        $id = $device->getId();
        $user_id = $device->getUser_id();
        $agreement_id = $device->getAgreement_id();
        $info_id = $device->getInfo_id();
        $model = $device->getModel();
        $product_nr = $device->getProduct_nr();
        $type = $device->getType();
        $serial_nr = $device->getSerial_nr();
        $cpu = $device->getCpu();
        $disc = $device->getDisc();
        $ram = $device->getRam();
        $graphics = $device->getGraphics();
        $os = $device->getOs();
        $active = $device->getActive();
        $onstock = $device->getOnstock();


        $params = array(
            'user_id' => $user_id,
            'agreement_id' => $agreement_id,
            'info_id' => $info_id,
            'model' => $model,
            'product_nr' => $product_nr,
            'type' => $type,
            'serial_nr' => $serial_nr,
            'cpu' => $cpu,
            'disc' => $disc,
            'ram' => $ram,
            'graphics' => $graphics,
            'os' => $os,
            'active' => $active,
            'onstock' => $onstock
        );

        $where = 'id="' . $id . '"';

        return $this->device->update($params, $where);
    }

    public function GetDeviceBySerial($string) {
        $string = "%" . $string . "%";
        $select = $this->device->select()
                        ->from('device')
                        ->where('serial_nr like ?', $string);

        return $this->device->fetchAll($select);
    }

    public function GetDeviceByModel($string) {
        $string = "%" . $string . "%";
        $select = $this->device->select()
                        ->from('device')
                        ->where('model like ?', $string);

        return $this->device->fetchAll($select);
    }

    public function GetDeviceByType($string) {
        $string = "%" . $string . "%";
        $select = $this->device->select()
                        ->from('device')
                        ->where('type like ?', $string);

        return $this->device->fetchAll($select);
    }

    public function GetDeviceByInfoId($id) {
        $select = $this->device->select()
                        ->from('device')
                        ->where('info_id = ?', $id);

        return $this->device->fetchRow($select);
    }

    public function GetDeviceByUserId($id) {
        $select = $this->device->select()
                        ->from('device')
                        ->where('user_id = ?', $id);

        return $this->device->fetchAll($select);
    }

    public function GetUserDevices($id) {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('device.id', 'i.got_nr', 'i.inv_type', 'device.active'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array())
                        ->where('device.active = 1')
                        ->where('device.user_id = ?', $id);
        $rows = $this->device->fetchAll($select);

        return($rows);
    }

    public function GetDeviceByAgreementId($id) {
        $select = $this->device->select()
                        ->from('device')
                        ->where('agreement_id = ?', $id);

        return $this->device->fetchAll($select);
    }

    public function search($cond) {
        $cond = '%' . $cond . '%';

        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('d' => 'device'),
                                array('d.id', 'i.got_nr', 'a.ticket_nr', 'i.host_name', 'i.inv_type', 'd.model', 'd.type', 'd.serial_nr', 'a.support_end', 'u.real_name', 'd.user_id', 'u.consultant', 'd.active'))
                        ->join(array('i' => 'info'),
                                'd.info_id = i.id', array())
                        ->join(array('u' => 'user'),
                                'd.user_id = u.id', array())
                        ->join(array('a' => 'agreement'),
                                'd.agreement_id = a.id', array())
                        ->where('i.got_nr like ?', $cond)
                        ->orWhere('i.host_name like ?', $cond)
                        ->orWhere('i.manufacturer like ?', $cond)
                        ->orWhere('u.real_name like ?', $cond)
                        ->orWhere('d.serial_nr like ?', $cond)
                        ->orWhere('d.model like ?', $cond)
                        ->orWhere('d.type like ?', $cond)
                        ->orWhere('a.order_nr like ?', $cond);

        return $this->device->fetchAll($select);
    }

    public function getDeviceCount() {
        $select = $this->device->select();
        $select->from('device', array('count(*) as amount'));
        $rows = $this->device->fetchAll($select);

        return($rows[0]->amount);
    }

    public function getActiveDeviceCount() {
        $select = $this->device->select();
        $select->from('device', array('count(*) as amount'))
                ->where('active = 1');
        $rows = $this->device->fetchAll($select);

        return($rows[0]->amount);
    }

    public function getActiveMonitorsCount() {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('device.active'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array('i.inv_type' => 'COUNT(device.active) AS amount'))
                        ->where('device.active = 1')
                        ->where('i.inv_type = "Monitor"');
        $rows = $this->device->fetchAll($select);

        return($rows[0]->amount);
    }

    public function getActiveLaptopsCount() {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('device.active'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array('i.inv_type' => 'COUNT(device.active) AS amount'))
                        ->where('device.active = 1')
                        ->where('i.inv_type = "Laptop"');
        $rows = $this->device->fetchAll($select);

        return($rows[0]->amount);
    }

    public function getActiveDesktopsCount() {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('device.active'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array('i.inv_type' => 'COUNT(device.active) AS amount'))
                        ->where('device.active = 1')
                        ->where('i.inv_type = "Desktop"');
        $rows = $this->device->fetchAll($select);

        return($rows[0]->amount);
    }

    public function getSupportEndDevices() {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('device.id', 'i.got_nr', 'a.ticket_nr', 'i.host_name', 'i.inv_type', 'device.model', 'device.type', 'device.serial_nr', 'a.support_end', 'u.real_name', 'device.user_id', 'u.consultant'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array())
                        ->join(array('u' => 'user'),
                                'device.user_id = u.id', array())
                        ->join(array('a' => 'agreement'),
                                'device.agreement_id = a.id', array())
                        ->where('device.active = 1')
                        ->where('datediff( a.support_end, NOW() ) < 30')
                        ->where('i.inv_type IN ("Laptop", "Desktop")')
                        ->order('a.support_end DESC');
        $rows = $this->device->fetchAll($select);

        return $rows;
    }

    public function getSupportEndCount() {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('device.active'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array())
                        ->join(array('a' => 'agreement'),
                                'device.agreement_id = a.id', array('a.id' => 'COUNT(a.id) AS amount'))
                        ->where('device.active = 1')
                        ->where('datediff( a.support_end, NOW( ) ) < 30')
                        ->where('i.inv_type IN ("Laptop", "Desktop")');
                        
        $rows = $this->device->fetchAll($select);

        return($rows[0]->amount);
    }

    public function getOnStockCount() {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('*'))
                        ->join(array('u' => 'user'),
                                'device.user_id = u.id', array('u.id' => 'COUNT(*) AS amount'))
                        ->where('device.onstock = 1')
                        ->where('device.active = 1');
        $rows = $this->device->fetchAll($select);

        return($rows[0]->amount);
    }

    public function getDevicesOnStock() {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('device.id', 'i.got_nr', 'a.ticket_nr', 'i.host_name', 'i.inv_type', 'device.model', 'device.type', 'device.serial_nr', 'a.support_end', 'u.real_name'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array())
                        ->join(array('u' => 'user'),
                                'device.user_id = u.id', array())
                        ->join(array('a' => 'agreement'),
                                'device.agreement_id = a.id', array())
                        ->where('onstock = 1')
                        ->where('active = 1')
                        ->order('got_nr DESC');

        return $this->device->fetchAll($select);
    }

    public function getActiveMonitors() {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('device.id', 'i.got_nr', 'a.ticket_nr', 'i.host_name', 'i.inv_type', 'device.model', 'device.type', 'device.serial_nr', 'a.support_end', 'u.real_name', 'device.user_id', 'u.consultant'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array())
                        ->join(array('u' => 'user'),
                                'device.user_id = u.id', array())
                        ->join(array('a' => 'agreement'),
                                'device.agreement_id = a.id', array())
                        ->where('active = 1')
                        ->where('i.inv_type = "Monitor"')
                        ->order('got_nr DESC');

        return $this->device->fetchAll($select);
    }

    public function getActiveLaptops() {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('device.id', 'i.got_nr', 'a.ticket_nr', 'i.host_name', 'i.inv_type', 'device.model', 'device.type', 'device.serial_nr', 'a.support_end', 'u.real_name', 'device.user_id', 'u.consultant'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array())
                        ->join(array('u' => 'user'),
                                'device.user_id = u.id', array())
                        ->join(array('a' => 'agreement'),
                                'device.agreement_id = a.id', array())
                        ->where('active = 1')
                        ->where('i.inv_type = "Laptop"')
                        ->order('got_nr DESC');

        return $this->device->fetchAll($select);
    }

    public function getActiveDesktops() {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('device.id', 'i.got_nr', 'a.ticket_nr', 'i.host_name', 'i.inv_type', 'device.model', 'device.type', 'device.serial_nr', 'a.support_end', 'u.real_name', 'device.user_id', 'u.consultant'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array())
                        ->join(array('u' => 'user'),
                                'device.user_id = u.id', array())
                        ->join(array('a' => 'agreement'),
                                'device.agreement_id = a.id', array())
                        ->where('active = 1')
                        ->where('i.inv_type = "Desktop"')
                        ->order('got_nr DESC');

        return $this->device->fetchAll($select);
    }

    public function GetDeviceInfoByType($type) {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('i.manufacturer', 'i.inv_type', 'device.model', 'device.product_nr', 'device.cpu', 'device.disc', 'device.ram', 'device.graphics', 'device.os'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array())
                        ->where('active = 1')
                        ->where('device.type = ?', $type);

        return $this->device->fetchAll($select);
    }

    public function GetDeviceInfoByModel($model) {
        $select = $this->device->select()->setIntegrityCheck(false)->distinct()
                        ->from(array('device'),
                                array('i.manufacturer', 'i.inv_type', 'device.model', 'device.product_nr', 'device.cpu', 'device.disc', 'device.ram', 'device.graphics', 'device.os', 'device.type'))
                        ->join(array('i' => 'info'),
                                'device.info_id = i.id', array())
                        ->where('active = 1')
                        ->where('device.model = ?', $model);

        return $this->device->fetchAll($select);
    }

    public function GetLast10Inserts() {
        $select = $this->device->select()->setIntegrityCheck(false)
                        ->from(array('d' => 'device'),
                                array('d.id', 'd.user_id', 'i.got_nr', 'a.ticket_nr', 'i.host_name', 'i.inv_type', 'd.model', 'd.type', 'd.serial_nr', 'a.support_end', 'u.real_name'))
                        ->join(array('i' => 'info'),
                                'd.info_id = i.id', array())
                        ->join(array('u' => 'user'),
                                'd.user_id = u.id', array())
                        ->join(array('a' => 'agreement'),
                                'd.agreement_id = a.id', array())
                        ->where('d.active = 1')
                        ->order('i.got_nr DESC')
                        ->limitpage(0, 10);

        return $this->device->fetchAll($select);

    }

}

?>
