<?php

/**
 * Description of UserService
 *
 * @author robert.gislason
 */
class Application_Service_InfoService{
    protected $db;
    protected $info;

    function __construct()
    {
        $this->info = new Application_Model_InfoTable();
    }

    public function NewInfo(Application_Model_Info $info)
    {
        $got_nr = $info->getGot_nr();
        $inst_date = $info->getInst_date();
        $host_name = $info->getHost_name();
        $usage = $info->getUsage();
        $inv_type = $info->getInv_type();
        $manufacturer = $info->getManufacturer();
        $maconomy_asset_nr = $info->getMaconomy_asset_nr();
        $note = $info->getNote();
        $password = $info->getPassword();

        
        $params = array(
          'got_nr'   => $got_nr,
          'inst_date'   => $inst_date,
          'host_name' => $host_name,
          'usage'  => $usage,
          'inv_type' => $inv_type,
          'manufacturer' => $manufacturer,
          'maconomy_asset_nr' => $maconomy_asset_nr,
          'note' => $note,
          'password' => $password
        );
        return $this->info->insert($params);

    }

    /*
     * Find info in database and return user if exists
     * Parameters: 
     * Returns: User if found otherwise null
     */
    public function GetAllInfo()
    {
        $select = $this->info->select();
        $select->order('got_nr');
        return $this->info->fetchAll($select);
    }

    public function GetAllNotes()
    {
        $select = $this->info->select()
                ->from('info', array('note'));
        return $this->info->fetchAll($select);
    }

    public function GetDeviceNote($got_nr)
    {
        $select = $this->info->select()
                ->from('info')
                ->where('got_nr = ?', $got_nr);

        return $this->info->fetchRow($select);
    }

    public function GetInfoById($id)
    {
        $select = $this->info->select()
                ->from('info')
                ->where('id = ?', $id);

        return $this->info->fetchRow($select);
    }

    public function UpdateInfo(Application_Model_Info $info)
    {
        $id = $info->getId();
        $got_nr = $info->getGot_nr();
        $inst_date = $info->getInst_date();
        $host_name = $info->getHost_name();
        $usage = $info->getUsage();
        $inv_type = $info->getInv_type();
        $manufacturer = $info->getManufacturer();
        $maconomy_asset_nr = $info->getMaconomy_asset_nr();
        $note = $info->getNote();
        $password = $info->getPassword();

        $params = array(
          'got_nr'   => $got_nr,
          'inst_date'   => $inst_date,
          'host_name' => $host_name,
          'usage'  => $usage,
          'inv_type'  => $inv_type,
          'manufacturer'  => $manufacturer,
          'maconomy_asset_nr'  => $maconomy_asset_nr,
          'note'  => $note,
          'password'  => $password
        );

        $where = 'id="'.$id.'"';

        return $this->info->update($params, $where);
    }

    public function GetInfoByGotNr($string)
    {
        $string = "%".$string."%";
        $select = $this->info->select()
                ->from('info')
                ->where('got_nr like ?', $string);

        return $this->info->fetchAll($select);
    }

    public function GetInfoByHostname($string)
    {
        $string = "%".$string."%";
        $select = $this->info->select()
                ->from('info')
                ->where('host_name like ?', $string);

        return $this->info->fetchAll($select);
    }

    public function GetInfoByManufacturer($string)
    {
        $string = "%".$string."%";
        $select = $this->info->select()
                ->from('info')
                ->where('manufacturer like ?', $string);

        return $this->info->fetchAll($select);
    }
    
}
?>
