<?php

/**
 * Description of UserService
 *
 * @author robert.gislason
 */
class Application_Service_UserService{
    protected $db;
    protected $user;

    function __construct()
    {
        $this->user = new Application_Model_UserTable();
    }

    public function NewUser(Application_Model_User $user)
    {
        $real_name = $user->getReal_name();
        $user_name = $user->getUser_name();                
        $responsible = $user->getResponsible();
        $department = $user->getDepartment();
        $consultant = $user->getConsultant();
        
        $params = array(
          'real_name'   => $real_name,
          'user_name'   => $user_name,
          'responsible' => $responsible,
          'department'  => $department,
          'consultant'  => $consultant
        );
        return $this->user->insert($params);

    }

    /*
     * Find user in database and return user if exists
     * Parameters: User
     * Returns: User if found otherwise null
     */
    public function GetAllUsers()
    {
        $select = $this->user->select()->distinct();
        $select->order('real_name');
        return $this->user->fetchAll($select);
    }

    public function GetAllUserNames()
    {
        $select = $this->user->select()
                ->from('user', array('real_name'));
        return $this->user->fetchAll($select);
    }

    public function FindUser($real_name)
    {
        $select = $this->user->select()
                ->from('user')
                ->where('real_name = ?', $real_name);

        return $this->user->fetchRow($select);
    }

    public function NewTestUser()
    {

        $params = array(
          'real_name'   => 'John Doe',
          'user_name'   => 'jonnydoe',
          'responsible' => 'Director of Does',
          'department'  => 'Does and donts'
        );
        $this->user->insert($params);
    }

    public function getUserById($id)
    {
        $select = $this->user->select()
                ->from('user')
                ->where('id = ?', $id);

        $row = $this->user->fetchRow($select);
        return $row->toArray();
    }

    public function UpdateUser(Application_Model_User $user)
    {
        $id = $user->getId();
        $real_name = $user->getReal_name();
        $user_name = $user->getUser_name();
        $responsible = $user->getResponsible();
        $department = $user->getDepartment();
        $consultant = $user->getConsultant();

        $params = array(
          'real_name'   => $real_name,
          'user_name'   => $user_name,
          'responsible' => $responsible,
          'department'  => $department,
          'consultant'  => $consultant
        );

        $where = 'id="'.$id.'"';

        return $this->user->update($params, $where);
    }

    public function GetUserByRealName($string)
    {
        $string = "%".$string."%";
        $select = $this->user->select()
                ->from('user')
                ->where('real_name like ?', $string);

        return $this->user->fetchAll($select);
    }

    public function GetUserByUserName($string)
    {
        $string = "%".$string."%";
        $select = $this->user->select()
                ->from('user')
                ->where('user_name = ?', $string);

        return $this->user->fetchAll($select);
    }

    public function CheckIfUserExists($real_name, $user_name)
    {
        $select = $this->user->select()->distinct()
                ->from('user')
                ->where('real_name = ?', $real_name)
                ->orwhere('user_name = ?', $user_name);

        return $this->user->fetchRow($select);
    }

    public function getUserCount()
    {
        $select = $this->user->select()->setIntegrityCheck(false)->distinct()
             ->from('user', 'COUNT(*) AS amount');
                    
        $rows = $this->user->fetchAll($select);

        return($rows[0]->amount);
    }

    public function SearchForUser($string)
    {
        $string = "%".$string."%";
        $select = $this->user->select()
                ->from(array('user'),
                array('user.id', 'user.real_name', 'user.user_name', 'user.responsible', 'user.department', 'user.consultant'))
                ->where('real_name like ?', $string)
                ->orwhere('user_name like ?', $string)
                ->orwhere('responsible like ?', $string)
                ->orwhere('department like ?', $string);

        return $this->user->fetchAll($select);
    }

    public function GetConsultants()
    {
        $select = $this->user->select()
                ->from('user')
                ->where('consultant = 1');

        return $this->user->fetchAll($select);
    }
}
?>
