<?php

/**
 * Description of AgreementService
 *
 * @author robert.gislason
 */
class Application_Service_AgreementService
{

    protected $db;
    protected $agreement;

    function __construct()
    {
        $this->agreement = new Application_Model_AgreementTable();
    }

    public function NewAgreement(Application_Model_Agreement $agreement)
    {
        $ticket_nr = $agreement->getTicket_nr();
        $support_agreement = $agreement->getSupport_agreement();
        $support_end = $agreement->getSupport_end();
        $bought_from = $agreement->getBought_from();
        $bought_on = $agreement->getBought_on();
        $order_nr = $agreement->getOrder_nr();

        $params = array(
            'ticket_nr' => $ticket_nr,
            'support_agreement' => $support_agreement,
            'support_end' => $support_end,
            'bought_from' => $bought_from,
            'bought_on' => $bought_on,
            'order_nr' => $order_nr
        );
        return $this->agreement->insert($params);
    }

    /*
     * Find agreement in database and return user if exists
     * Parameters:
     * Returns: All agreements
     */

    public function GetAllAgreements()
    {
        $select = $this->agreement->select();
        $select->order('order_nr');
        return $this->agreement->fetchAll($select);
    }

    public function GetAllAgreementOrdernr()
    {
        $select = $this->agreement->select()
                        ->from('agreement', array('order_nr'));
        return $this->agreement->fetchAll($select);
    }

    public function FindAgreement($order_nr)
    {
        $select = $this->agreement->select()
                        ->from('agreement')
                        ->where('order_nr = ?', $order_nr);

        return $this->agreement->fetchRow($select);
    }

    public function GetAgreementById($id)
    {
        $select = $this->agreement->select()
                        ->from('agreement')
                        ->where('id = ?', $id);

        return $this->agreement->fetchRow($select);
    }

    public function UpdateAgreement(Application_Model_Agreement $agreement)
    {
        $id = $agreement->getId();
        $ticket_nr = $agreement->getTicket_nr();
        $support_agreement = $agreement->getSupport_agreement();
        $support_end = $agreement->getSupport_end();
        $bought_from = $agreement->getBought_from();
        $bought_on = $agreement->getBought_on();
        $order_nr = $agreement->getOrder_nr();

        $params = array(
            'ticket_nr' => $ticket_nr,
            'support_agreement' => $support_agreement,
            'support_end' => $support_end,
            'bought_from' => $bought_from,
            'bought_on' => $bought_on,
            'order_nr' => $order_nr
        );

        $where = 'id="' . $id . '"';

        return $this->agreement->update($params, $where);
    }

    public function GetAgreementByOrderNr($string)
    {
        $string = "%".$string."%";
        $select = $this->agreement->select()
                        ->from('agreement')
                        ->where('order_nr like ?', $string);

        return $this->agreement->fetchAll($select);
    }
}

?>
