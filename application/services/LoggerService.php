<?php

/**
 * Description of AgreementService
 *
 * @author robert.gislason
 */
class Application_Service_LoggerService
{

    protected $db;
    protected $logger;

    function __construct()
    {
        $this->logger = new Application_Model_LoggerTable();
    }

    public function NewLog(Application_Model_Logger $logger)
    {
        $ticket_nr = $logger->getTicket_nr();
        $support_agreement = $logger->getSupport_agreement();
        $support_end = $logger->getSupport_end();
        $bought_from = $logger->getBought_from();
        $bought_on = $logger->getBought_on();
        $order_nr = $logger->getOrder_nr();
        $got_nr = $logger->getGot_nr();
        $installation_date = $logger->getInstallation_date();
        $hostname = $logger->getHostname();
        $usage = $logger->getUsage();
        $inv_type = $logger->getInv_type();
        $manufacturer = $logger->getManufacturer();
        $maconomy = $logger->getMaconomy();
        $note = $logger->getNote();
        $password = $logger->getPassword();
        $real_name = $logger->getReal_name();
        $user_name = $logger->getUser_name();
        $responsible = $logger->getResponsible();
        $department = $logger->getDepartment();
        $consultant = $logger->getConsultant();
        $model = $logger->getModel();
        $product_nr = $logger->getProduct_nr();
        $type = $logger->getType();
        $serial_nr = $logger->getSerial_nr();
        $cpu = $logger->getCpu();
        $disc = $logger->getDisc();
        $ram = $logger->getRam();
        $graphics = $logger->getGraphics();
        $os = $logger->getOs();
        $active = $logger->getActive();
        $onstock = $logger->getOnstock();
        $datetime = $logger->getDatetime();
        $action = $logger->getAction();

        $params = array(
            'ticket_nr' => $ticket_nr,
            'support_agreement' => $support_agreement,
            'support_end' => $support_end,
            'bought_from' => $bought_from,
            'bought_on' => $bought_on,
            'order_nr' => $order_nr,
            'got_nr' => $got_nr,
            'installation_date' => $installation_date,
            'hostname' => $hostname,
            'usage' => $usage,
            'inv_type' => $inv_type,
            'manufacturer' => $manufacturer,
            'maconomy' => $maconomy,
            'note' => $note,
            'password' => $password,
            'real_name' => $real_name,
            'user_name' => $user_name,
            'responsible' => $responsible,
            'department' => $department,
            'consultant' => $consultant,
            'model' => $model,
            'product_nr' => $product_nr,
            'type' => $type,
            'serial_nr' => $serial_nr,
            'cpu' => $cpu,
            'disc' => $disc,
            'ram' => $ram,
            'graphics' => $graphics,
            'os' => $os,
            'active' => $active,
            'onstock' => $onstock,
            'datetime' => $datetime,
            'action' => $action
        );
        return $this->logger->insert($params);
    }

    /*
     * Find agreement in database and return user if exists
     * Parameters:
     * Returns: All agreements
     */

    public function GetAllLogs()
    {
        $select = $this->logger->select();
        $select->order('id DESC')
			   ->limitpage(0, 50);
        return $this->logger->fetchAll($select);
    }

    public function GetLogByDate($datetime)
    {
        $select = $this->logger->select()
                        ->from('logger')
                        ->where('datetime = ?', $datetime);

        return $this->logger->fetchAll($select);
    }

    public function GetLogByGotnr($gotnr)
    {
        $gotnr = '%'.$gotnr.'%';
        $select = $this->logger->select()
                        ->from('logger')
                        ->where('got_nr like ?', $gotnr);

        return $this->logger->fetchAll($select);
    }
    
}

?>
